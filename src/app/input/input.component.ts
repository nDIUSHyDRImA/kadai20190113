import { Component, OnInit } from '@angular/core';
import { ResultComponent } from '../result/result.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.sass']
})
export class InputComponent implements OnInit {
  inputedNumber: number;
  hasError: boolean;

  constructor() {
    this.hasError = false;
  }

  ngOnInit() {
  }

  errorStatusListener(hasError: boolean) {
    this.hasError = hasError;
  }

}
